********
Epilogue
********

This completes our tour of GPS, the GNAT Programming Studio. We hope
this tutorial gave you a good overview of the general capabilities of
GPS. A non-exhaustive list of the features not mentioned in this
document includes:

* Documentation generation
* Automatic generation of body files
* Pretty printing
* Visual comparison of files
* Version control
* Flexible multiple document interface
* Code coverage
* Coding standard verification
* Extensive customization through Python

For more information, please see the *GPS User's Guide* or the GPS `Learn` view,
which displays the common actions available from the current context.
